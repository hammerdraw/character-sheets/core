from dataclasses import dataclass
from typing import *

from dataclasses_config import Config, Settings
from hammerdraw.util import ImageType, get_color
from PIL import ImageDraw

from .mixins.image_row_mixin import ImageRowMixin
from .mixins.multipart_module_mixin import MultiPartModuleMixin

@dataclass(frozen=Settings.frozen)
class CaptionParameters(Config):
    heights: List[int]
    underscore_color: Union[List[int], str, int]
    underscore_width: int

class CaptionModule(MultiPartModuleMixin[CaptionParameters], ImageRowMixin):
    base_name = 'caption'
    config_class = CaptionParameters
    
    def _compile(self, base: ImageType):
        params = self._get_parameters()
        
        drawer = ImageDraw.ImageDraw(base)
        line_style = dict(fill=get_color(params.underscore_color), width=params.underscore_width)
        
        for y in params.heights:
            drawer.line([ (0, y), (self.width, y) ], **line_style)
        
        return 0

__all__ = \
[
    'CaptionModule',
    'CaptionParameters',
]
