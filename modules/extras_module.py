from dataclasses import dataclass, field
from typing import *

from dataclasses_config import Config, Path, Settings
from hammerdraw.util import ImageType, get_color
from PIL import ImageDraw

from .mixins.image_row_mixin import ImageRowMixin, ImageObjectAttributes
from .mixins.multipart_module_mixin import MultiPartModuleMixin

@dataclass(frozen=Settings.frozen)
class ExtrasBlockParameters(Config):
    title: str
    min: int
    max: int
    
    row_height: int
    header_height: int
    header_left_offset: int
    header_right_offset: int
    header_interval: int
    
    point_image_width: int
    point_purchased_image: str
    point_free_image: str
    points_width: int
    
    button_image: str
    underscore_color: Union[List[int], str, int]
    underscore_width: int
    
    predefined: List[Tuple[str, Optional[int]]] = field(default_factory=list)
    comments: List[str] = field(default_factory=list)
    blank_rows: int = 0
    skip_header: bool = False
    inline: bool = False
    
    def __post_init__(self):
        super().__post_init__()
        for i in range(len(self.predefined)):
            p = self.predefined[i]
            if (isinstance(p, tuple) and all(isinstance(c, str) for c in p)):
                p = ''.join(p)
            if (isinstance(p, str)):
                self.predefined[i] = (p, None)

class ExtrasBlockModule(MultiPartModuleMixin[ExtrasBlockParameters], ImageRowMixin):
    base_name = 'extras'
    config_class = ExtrasBlockParameters
    
    def _get_image_objects(self, params: ExtrasBlockParameters, min: int = None, max: int = None) -> List[ImageObjectAttributes]:
        if (min is None):
            min = params.min
        if (max is None):
            max = params.max
        
        image_objects = \
        [
            ImageObjectAttributes(min, params.point_purchased_image),
            ImageObjectAttributes(max - min, params.point_free_image),
        ]
        return image_objects
    
    def _compile(self, base: ImageType):
        params = self._get_parameters()
        total_rows = len(params.predefined) + len(params.comments) + params.blank_rows
        
        w = self.width - params.points_width
        y = 0
        
        if (not params.skip_header):
            self.parent.insert_image_scaled(base, (0, y, self.width, 0), Path(self.parent.sources_directory, params.button_image))
            header_drawer = self.get_text_drawer(base, 'fonts/header', 'fonts/headerStyle')
            header_drawer.print_in_region((params.header_left_offset, y, (self.width if (not params.inline) else w) - params.header_right_offset - params.header_left_offset, params.header_height), params.title, max_lines=1)
            y += params.header_height + params.header_interval
        
        image_objects = self._get_image_objects(params)
        
        if (params.inline):
            self.insert_image_row(base, image_objects, image_width=params.point_image_width, region=(w, 0, params.points_width, params.header_height))
        else:
            drawer = ImageDraw.ImageDraw(base)
            line_style = dict(fill=get_color(params.underscore_color), width=params.underscore_width)
            
            if (params.predefined):
                td = self.get_text_drawer(base, 'fonts/predefined', 'fonts/predefinedStyle')
                for predefined, purchased in params.predefined:
                    if (purchased is not None):
                        _io = self._get_image_objects(params, purchased)
                    else:
                        _io = image_objects
                    
                    td.print_in_region((0, y, w, params.row_height), predefined, max_lines=1)
                    self.insert_image_row(base, _io, image_width=params.point_image_width, region=(w, y, params.points_width, params.row_height))
                    y += params.row_height
            
            if (params.comments):
                td = self.get_text_drawer(base, 'fonts/comment', 'fonts/commentStyle')
                for comment in params.comments:
                    self.insert_image_row(base, image_objects, image_width=params.point_image_width, region=(w, y, params.points_width, params.row_height))
                    y += params.row_height
                    drawer.line([ (0, y), (self.width - params.points_width, y) ], **line_style)
                    td.print_in_region((0, y, w, params.row_height), comment, max_lines=1)
            
            if (params.blank_rows):
                for _ in range(params.blank_rows):
                    self.insert_image_row(base, image_objects, image_width=params.point_image_width, region=(w, y, params.points_width, params.row_height))
                    y += params.row_height
                    drawer.line([ (0, y), (self.width - params.points_width, y) ], **line_style)
        
        return y

__all__ = \
[
    'ExtrasBlockParameters',
    'ExtrasBlockModule',
]
