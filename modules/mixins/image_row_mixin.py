from abc import ABC
from dataclasses import dataclass
from typing import List, Optional

from dataclasses_config import Path, Config, Settings
from hammerdraw.compiler_modules import ModuleBase
from hammerdraw.util import Region, ImageType

@dataclass(frozen=Settings.frozen)
class ImageObjectAttributes(Config):
    count: int
    image: Optional[str]
    """ This might be null. But it has to be defined explicitly """

class ImageRowMixin(ModuleBase, ABC):
    def insert_image_row(self, base: ImageType, objects: List[ImageObjectAttributes], *, image_width: int, region: Optional[Region] = None, fixed_width: Optional[int] = None):
        total_count = sum(obj_type.count for obj_type in objects)
        
        if (region is None):
            region = (0, 0, self.width, self.height)
        
        x0, y0, w, h = region
        
        x1 = x0 + image_width // 2
        x2 = x0 + w - image_width // 2
        _x = x1
        _dx = (x2 - x1) // max(total_count - 1, 1)
        
        if (fixed_width is not None and fixed_width < _dx):
            _dx = image_width + fixed_width
            _x = (w - _dx * (total_count - 1)) // 2
        
        _y = y0 + h // 2
        for _obj_type in objects:
            # If image is None, print "space"
            if (_obj_type.image is None):
                _x += _dx * _obj_type.count
                continue
            
            img_path = Path(self.parent.sources_directory, _obj_type.image)
            img_width, _ = self.parent.get_image_size(img_path)
            scale = 1.0 * image_width / img_width 
            
            for i in range(_obj_type.count):
                self.parent.insert_image_centered(base_image=base, position=(_x, _y), image_path=img_path, scale=scale)
                _x += _dx
        
        self.logger.debug("Image row printed")

__all__ = \
[
    'ImageRowMixin',
    'ImageObjectAttributes',
]
