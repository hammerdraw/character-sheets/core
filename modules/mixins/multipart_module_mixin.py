import warnings
from typing import *

from dataclasses_config import Config
from .parameterized_module_mixin import ParameterizedModuleMixin

_MISSING = object()
T = TypeVar('T', bound=Config)
class MultiPartModuleMixin(ParameterizedModuleMixin[T], Generic[T]):
    base_name: str
    
    def __init__(self, *args, name: str = None, **kwargs):
        self.module_name = name or self.module_name
        if (self.module_name is None):
            raise ValueError("Module name is missing!")
        
        self.module_config_prefix += f'{self.base_name}/'
        super().__init__(*args, **kwargs)
    
    def _get_parameters(self) -> T:
        from_config: Dict[str, Any] = self.get_from_config('', dict())
        from_raw_root: Dict[str, Any] = self.get_from_raw(f'{self.base_name}', dict())
        from_raw_specific: Dict[str, Any] = self.get_from_raw(f'{self.base_name}/{self.module_name}', dict())
        
        params = self._merge_dicts(from_raw_specific, from_raw_root, from_config)
        return self.config_class.from_dict(params)
    
    def get_from_config(self, key: str, default = None):
        warnings.warn("This method is going to be deprecated, use the Config subclasses instead", DeprecationWarning, 2)
        
        curr_value = super().get_from_config(key, default=_MISSING)
        root_value = super().get_from_config(f'../{key}', default=default)
        if (curr_value is not _MISSING):
            if (isinstance(curr_value, dict) and isinstance(root_value, dict)):
                return self._merge_dicts(curr_value, root_value)
            else:
                return curr_value
        else:
            return root_value
    
    def compile(self, *args, **kwargs):
        result = super().compile(*args, **kwargs)
        self.logger.info(f"{self.base_name.title()}/{self.module_name.title()} printed")
        return result

__all__ = \
[
    'MultiPartModuleMixin',
]
