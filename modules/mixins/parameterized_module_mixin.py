import warnings
from copy import deepcopy
from functools import reduce
from typing import *

from dataclasses_config import Config
from hammerdraw.compiler_modules import ModuleBase

_MISSING = object()
T = TypeVar('T', bound=Config)
class ParameterizedModuleMixin(ModuleBase, Generic[T]):
    config_class: Type[T]
    
    K = TypeVar('K')
    V = TypeVar('V')
    @classmethod
    def _merge_dicts(cls, *dicts: Dict[K, V]) -> Dict[K, V]:
        for d in dicts:
            if (not isinstance(d, dict)):
                raise TypeError(f"All arguments must be dicts, got {d!r}: {type(d)}")
        
        if (len(dicts) == 1):
            return dicts[0]
        
        elif (len(dicts) == 2):
            left = deepcopy(dicts[0])
            right = deepcopy(dicts[1])
            
            for k, v in right.items():
                if (k in left and isinstance(left[k], dict) and isinstance(right[k], dict)):
                    left[k] = cls._merge_dicts(left[k], right[k])
                elif (k not in left):
                    left[k] = right[k]
                else:
                    pass
            
            return left
        
        else:
            return reduce(cls._merge_dicts, dicts)
    
    del K, V
    
    def __init__(self, *args, name: str = None, **kwargs):
        self.module_name = name or self.module_name
        if (self.module_name is None):
            raise ValueError("Module name is missing!")
        
        super().__init__(*args, **kwargs)
    
    def _get_parameters(self) -> T:
        from_config: Dict[str, Any] = self.get_from_config('', dict())
        from_raw: Dict[str, Any] = self.get_from_raw(f'{self.module_name}', dict())
        
        params = self._merge_dicts(from_raw, from_config)
        return self.config_class.from_dict(params)
    
    def get_from_config(self, *args, **kwargs):
        warnings.warn("This method is going to be deprecated, use the Config subclasses instead", DeprecationWarning, 2)
        return super().get_from_config(*args, **kwargs)

__all__ = \
[
    'ParameterizedModuleMixin',
]
