from dataclasses import dataclass
from typing import *

from dataclasses_config import Config, Settings
from hammerdraw.util import ImageType

from .mixins.image_row_mixin import ImageRowMixin, ImageObjectAttributes
from .mixins.multipart_module_mixin import MultiPartModuleMixin

@dataclass(frozen=Settings.frozen)
class SkillBlockParameters(Config):
    title: str
    skills: List[str]
    min: int
    max: int
    
    row_height: int
    header_height: int
    point_image_width: int
    point_purchased_image: str
    point_free_image: str
    points_width: int

class SkillBlockModule(MultiPartModuleMixin[SkillBlockParameters], ImageRowMixin):
    base_name = 'skills'
    config_class = SkillBlockParameters
    
    def _compile(self, base: ImageType):
        params = self._get_parameters()
        
        header_drawer = self.get_text_drawer(base, 'fonts/header', 'fonts/headerStyle')
        header_drawer.print_in_region((0, 0, self.width, params.header_height), params.title, max_lines=1)
        
        w = self.width - params.points_width
        y = params.header_height
        td = self.get_text_drawer(base, 'fonts/skill', 'fonts/skillStyle')
        image_objects = \
        [
            ImageObjectAttributes(params.min, params.point_purchased_image), 
            ImageObjectAttributes(params.max - params.min, params.point_free_image),
        ]
        for skill in params.skills:
            td.print_in_region((0, y, self.width - params.points_width, params.row_height), skill, max_lines=1)
            self.insert_image_row(base, image_objects, image_width=params.point_image_width, region=(w, y, params.points_width, params.row_height))
            y += params.row_height
        
        return y

__all__ = \
[
    'SkillBlockParameters',
    'SkillBlockModule',
]
