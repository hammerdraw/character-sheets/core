from dataclasses import dataclass
from typing import *

from dataclasses_config import Config, Settings
from hammerdraw.text_drawer import TextAlignment
from hammerdraw.util import ImageType

from .mixins.image_row_mixin import ImageRowMixin, ImageObjectAttributes
from .mixins.multipart_module_mixin import MultiPartModuleMixin

@dataclass(frozen=Settings.frozen)
class StatsBlockParameters(Config):
    title: str
    header_height: int
    row_height: int
    image_width: int
    
    constant: Optional[List[ImageObjectAttributes]] = None
    temporary: Optional[List[ImageObjectAttributes]] = None
        
    left_comment: Optional[str] = None
    right_comment: Optional[str] = None
    comment_height: Optional[int] = None

class StatsBlockModule(MultiPartModuleMixin[StatsBlockParameters], ImageRowMixin):
    base_name = 'stats'
    config_class = StatsBlockParameters
    
    def _compile(self, base: ImageType):
        params = self._get_parameters()
        
        header_drawer = self.get_text_drawer(base, 'fonts/header', 'fonts/headerStyle')
        header_drawer.print_in_region((0, 0, self.width, params.header_height), params.title)
        y = params.header_height
        
        if (params.constant is not None):
            self.insert_image_row(base, params.constant,  region=(0, y, self.width, params.row_height), image_width=params.image_width)
            y += params.header_height
        
        if (params.left_comment or params.right_comment):
            comment_drawer = self.get_text_drawer(base, 'fonts/comment', 'fonts/commentStyle')
            if (params.left_comment):
                comment_drawer.set_font(horizontal_alignment=TextAlignment.Left)
                comment_drawer.print_in_region((0, y, self.width, params.comment_height), params.left_comment)
            if (params.right_comment):
                comment_drawer.set_font(horizontal_alignment=TextAlignment.Right)
                comment_drawer.print_in_region((0, y, self.width, params.comment_height), params.right_comment)
            y += params.comment_height
        
        if (params.temporary is not None):
            self.insert_image_row(base, params.temporary, region=(0, y, self.width, params.row_height), image_width=params.image_width)
            y += params.header_height
        
        return y

__all__ = \
[
    'StatsBlockParameters',
    'StatsBlockModule',
]
